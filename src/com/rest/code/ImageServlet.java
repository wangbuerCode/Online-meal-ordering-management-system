package com.rest.code;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//一定要加/
@WebServlet("/ImageServlet")
public class ImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	/*
	 * 干扰线的颜色
	 * */
	private Color interLine(int low, int high) {
		if (low > 255)
			low = 255;
		if (high > 255)
			high = 255;
		if (low < 0)
			low = 0;
		if (high < 0)
			high = 0;
		
		int interval = high - low;
		int r = low + (int) (Math.random() * interval);
		int g = low + (int) (Math.random() * interval);
		int b = low + (int) (Math.random() * interval);
		return new Color(r, g, b);
	}

	/*
	 * 响应用户的GET请求
	 * */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ServletContext scontext=this.getServletContext();
		System.out.println(scontext.getAttribute("侯"));
		
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");

		//1.创建一个画布(找了一张纸)
		BufferedImage bfi = new BufferedImage(80, 25, BufferedImage.TYPE_INT_RGB);
		//2.一支画笔
		Graphics g = bfi.getGraphics();
		// 		X  Y width height
		g.fillRect(0, 0, 80, 25);
		//画的内容
		char[] ch = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
		Random r = new Random();
		int index;
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < 4; i++) {
			index = r.nextInt(ch.length);
			//设置画的颜色 RGB
			g.setColor(new Color(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
			Font font = new Font("courier new", 30, 20);
			//设置字体
			g.setFont(font);
			
			//开始写字
			g.drawString(ch[index] + "", (i * 20) + 2, 23);
			sb.append(ch[index]);
		}

		//添加噪点
//		int area = (int) (0.02 * 80 * 25);
//		for (int i = 0; i < area; ++i) {
//			int x = (int) (Math.random() * 80);
//			int y = (int) (Math.random() * 25);
//			bfi.setRGB(x, y, (int)(Math.random() * 255));
//		}

		//设置验证码中的干扰线
//		for (int i = 0; i < 6; i++) {
//			// 随机获取干扰线的起点和终点
//			int xstart = (int) (Math.random() * 80);
//			int ystart = (int) (Math.random() * 25);
//			int xend = (int) (Math.random() * 80);
//			int yend = (int) (Math.random() * 25);
		//保存到session，以便和用户输入的值进行比较
			g.setColor(interLine(1, 255));
//			g.drawLine(xstart, ystart, xend, yend);
//		}
			HttpSession session	= request.getSession();
			session.setAttribute("checkcode", sb.toString());
		
		//写到输出流
		ImageIO.write(bfi, "JPG", response.getOutputStream()); 
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		 doGet(request,response);
	}

	
}

