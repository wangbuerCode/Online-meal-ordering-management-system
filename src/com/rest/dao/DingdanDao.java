package com.rest.dao;

import java.util.List;

import com.rest.model.Dingdan;
public interface DingdanDao {
	public boolean insertuser(Dingdan dd,String time);
	
	public boolean deletemsg(String name);
	
	public List<Dingdan> selectall(String name);
	
	
}
