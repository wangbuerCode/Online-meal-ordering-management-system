package com.rest.dao;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.rest.model.Dingdan;
import com.rest.utils.JDBCUtils;

public class DingdanDaoImpl implements DingdanDao{
	JdbcTemplate jt = new JdbcTemplate(JDBCUtils.getDataSource());
	@Override
	public boolean insertuser(Dingdan dd,String time) {//生成订单
		String sql = "insert dingdan values (?,?,?) ";
		int re = jt.update(sql,dd.getD_uname(),time,dd.getD_zongjine());
		return re>0;
	}
	@Override
	public boolean deletemsg(String name) {//购买后删除之前菜品
		String sql = "delete from shopcar where s_uname = ?";
		int re = jt.update(sql,name);
		return re>0;
	}
	@Override
	public List<Dingdan> selectall(String name) {//查询所有订单
		String sql = "select * from dingdan where d_uname = ? ";
		List<Dingdan> list = jt.query(sql,new BeanPropertyRowMapper<Dingdan>(Dingdan.class),name);
		return list;
	}

}
