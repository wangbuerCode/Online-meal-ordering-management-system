package com.rest.dao;

import java.util.List;

import com.rest.model.Mibao;
import com.rest.model.User;

public interface FindDao {
	
	public List<User> Find(User user);
	
	public List<Mibao> Fpwd( int id);
}
