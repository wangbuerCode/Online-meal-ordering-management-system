package com.rest.dao;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.rest.model.Mibao;
import com.rest.model.User;
import com.rest.utils.JDBCUtils;

public class FindDaoImpl implements FindDao{
	JdbcTemplate jt = new JdbcTemplate(JDBCUtils.getDataSource());
	@Override
	public List<User> Find(User user) {
			String sql = "select * from user where u_name = ? and u_telenum=?";
			List<User> list = jt.query(sql,new BeanPropertyRowMapper<User>(User.class),user.getU_name(),user.getU_telenum());
		return list;
	}
	@Override
	public List<Mibao> Fpwd(int id) {
		String sql = "select * from mibao m,user u where m_id=u_id and m_id=?";
		List <Mibao> list =jt.query(sql, new BeanPropertyRowMapper<Mibao>(Mibao.class),id);
		return list;
	}

}
