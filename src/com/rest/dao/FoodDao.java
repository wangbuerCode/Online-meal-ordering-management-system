package com.rest.dao;

import java.util.List;

import com.rest.model.Food;

public interface FoodDao {
	public List<Food> showAll();
	
	public List<Food> showOne(int id);
}
