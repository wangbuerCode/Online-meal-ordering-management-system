package com.rest.dao;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.rest.model.Food;
import com.rest.utils.JDBCUtils;

public class FoodDaoImpl implements FoodDao{
	JdbcTemplate jt = new JdbcTemplate(JDBCUtils.getDataSource());

	@Override
	public List<Food> showAll() {
		String sql = "select * from food order by f_class";
		List<Food>list = jt.query(sql,new BeanPropertyRowMapper<Food>(Food.class));
		return list;
	}

	@Override
	public List<Food> showOne(int id) {
		String sql = "select * from food where f_class = ? ";
	    List<Food> list = jt.query(sql, new BeanPropertyRowMapper<Food>(Food.class),id);
		return list;
	}
	
	
	
	
}
