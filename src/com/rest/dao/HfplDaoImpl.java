package com.rest.dao;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import com.rest.model.Hfpl;
import com.rest.utils.JDBCUtils;

public class HfplDaoImpl implements HfplDao{

	JdbcTemplate jt = new JdbcTemplate(JDBCUtils.getDataSource());
	
	@Override
	public List<Hfpl> showhf() {
		String sql = "select * from hfpl order by h_time";
		List<Hfpl>list = jt.query(sql,new BeanPropertyRowMapper<Hfpl>(Hfpl.class));
		return list;
	}

	@Override
	public boolean insertpl(int id, String name, String cment, String time) {
		String sql = "insert hfpl values(?,?,?,?)";
		int re = jt.update(sql,id,name,cment,time);
		return re>0;
	}

}
