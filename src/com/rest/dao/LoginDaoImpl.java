package com.rest.dao;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.rest.model.User;
import com.rest.utils.JDBCUtils;

public class LoginDaoImpl implements Logindao{

	//创建工具对象
	JdbcTemplate jt = new JdbcTemplate(JDBCUtils.getDataSource());

	@Override
	public List<User> login(User user) {
		String sql = "select * from user where u_name = ? and u_pwd=?";
		List<User> list = jt.query(sql,new BeanPropertyRowMapper<User>(User.class),user.getU_name(),user.getU_pwd());
		return list;
	}
	

	@Override
	public List<User> loginname() {
		String sql="select * from user";
		List<User>list=jt.query(sql, new BeanPropertyRowMapper<User>(User.class));
		return list;
	}


	@Override
	public List<User> login1(User user) {
		String sql = "select * from user where u_telenum = ? and u_pwd=?";
		List<User> list = jt.query(sql,new BeanPropertyRowMapper<User>(User.class),user.getU_telenum(),user.getU_pwd());
		return list;
	}


	@Override
	public List<User> ShowLogin(String name) {
		String sql = "select * from user u,vip v where u_vip=v_vip and u_name = ?";
		List<User> list = jt.query(sql, new BeanPropertyRowMapper<User>(User.class),name);
		return list;
	}
	
	@Override
	public List<User> ShowLogin1(String name) {
		String sql = "select * from user u,vip v where u_vip=v_vip and u_telenum = ?";
		List<User> list = jt.query(sql, new BeanPropertyRowMapper<User>(User.class),name);
		return list;
	}




	

	
	


}
	
	


