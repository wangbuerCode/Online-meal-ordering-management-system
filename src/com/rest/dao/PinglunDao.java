package com.rest.dao;

import java.util.List;

import com.rest.model.Hfpl;
import com.rest.model.Pinglun;

public interface PinglunDao {
	public List<Pinglun> showpl();
	
	public boolean insertpl(String name,String cment,String time);//发表留言
	
	public List<Pinglun> showmy(String name);//查看自己的评论
	
	public boolean delectmypl(int id);
	
	public List<Hfpl> showhf(String name);
	
	public boolean delecthf(String time,String cment);
}
