package com.rest.dao;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.rest.model.Hfpl;
import com.rest.model.Pinglun;
import com.rest.utils.JDBCUtils;

public class PinglunDaoImpl implements PinglunDao{
	JdbcTemplate jt = new JdbcTemplate(JDBCUtils.getDataSource());
	@Override
	public List<Pinglun> showpl() {
		String sql = "select * from comment order by c_id desc";
		List<Pinglun>list = jt.query(sql, new BeanPropertyRowMapper<Pinglun>(Pinglun.class));
		return list;
	}
	@Override
	public boolean insertpl(String name, String cment, String time) {
		String sql = "insert comment values(0,?,?,?)";
		int re = jt.update(sql,name,cment,time);
		return re>0;
	}
	@Override
	public List<Pinglun> showmy(String name) {
		String sql = "select * from comment where c_name = ?";
	    List<Pinglun>list =	jt.query(sql,new BeanPropertyRowMapper<Pinglun>(Pinglun.class),name);
		return list;
	}
	@Override
	public boolean delectmypl(int id) {
		String sql = "delete from comment where c_id=?";
		int re = jt.update(sql,id);
		return re>0;
	}
	@Override
	public List<Hfpl> showhf(String name) {
		String sql = "select * from comment c,hfpl h where c_id=h_id and h_name=?";
		List<Hfpl>list = jt.query(sql,new BeanPropertyRowMapper<Hfpl>(Hfpl.class),name);
		return list;
	}
	@Override
	public boolean delecthf(String cment, String time) {
		String sql = "delete from hfpl where h_name=? and h_time = ?";
		int re = jt.update(sql,cment,time);
		return re>0;
	}

	
}
