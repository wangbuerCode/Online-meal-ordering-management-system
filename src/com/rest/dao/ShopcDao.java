package com.rest.dao;

import java.util.List;

import com.rest.model.ShoPCar;
import com.rest.model.User;
import com.rest.model.Xxdd;

public interface ShopcDao {
	public boolean addCaidan(ShoPCar sc);
	
	public void update(String name);
	
	public List<ShoPCar> selectall(String uname);
	
	public List<ShoPCar> Showall(String name);//显示所有选中的菜
	
	public boolean delect(int id);//删除选中菜品
	
	public boolean addnum(String name,String uname);//点击加菜
	public boolean minnum(String name,String uname);//减菜
	public boolean deletenum(String name,String uname);//删除菜
	
	public List<User> selectvip(String name);
	
	public boolean insertXxdd(Xxdd xd,String time);
	
	public List<Xxdd> selectxd(Xxdd xd);
	
}
