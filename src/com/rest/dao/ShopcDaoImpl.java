package com.rest.dao;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import com.rest.model.ShoPCar;
import com.rest.model.User;
import com.rest.model.Xxdd;
import com.rest.utils.JDBCUtils;

public class ShopcDaoImpl implements ShopcDao{
	JdbcTemplate jt = new JdbcTemplate(JDBCUtils.getDataSource());
	@Override
	public boolean addCaidan(ShoPCar sc) {
		String sql = "insert shopcar values(0,?,?,?,?,1,null) ";
		int re = jt.update(sql,sc.getS_foodimg(),sc.getS_uname(),sc.getS_foodname(),sc.getS_foodprice());
		return re>0;
	}
	@Override
	public void update(String name) {
		String sql ="update shopcar set s_num=s_num+1 where s_foodname = ?";
		jt.update(sql,name);
	}
	@Override
	public List<ShoPCar> selectall(String uname) {
		String sql = "select s_foodname from shopcar where s_uname = ?";
		List<ShoPCar>list = jt.query(sql, new BeanPropertyRowMapper<ShoPCar>(ShoPCar.class),uname);
		return list;
	}
	@Override
	public List<ShoPCar> Showall(String name) {
		String sql = "select * from shopcar where s_uname = ?";
		List<ShoPCar>list = jt.query(sql, new BeanPropertyRowMapper<ShoPCar>(ShoPCar.class),name);
		return list;
	}
	@Override
	public boolean delect(int id) {
		String sql = "delete from shopcar where s_id = ?";
		int re = jt.update(sql,id);
		return re>0;
	}
	@Override
	public boolean addnum(String name,String uname) {
		String sql = "update shopcar set s_num=s_num+1 where s_foodname = ? and s_uname=?";
		int re = jt.update(sql,name,uname);
		return re>0;
	}
	@Override
	public boolean minnum(String name, String uname) {
		String sql = "update shopcar set s_num=s_num-1 where s_foodname = ? and s_uname=?";
		int re = jt.update(sql,name,uname);
		return re>0;
	}
	@Override
	public boolean deletenum(String name, String uname) {
		String sql = "delete from shopcar where s_foodname = ? and s_uname = ?";
		int re = jt.update(sql,name,uname);
		return re>0;
	}
	@Override
	public List<User> selectvip(String name) {
		String sql = "SELECT v_discount,v_state FROM USER u,vip v WHERE u.u_vip =v.v_vip AND u.u_name=?";
		List<User> list = jt.query(sql,new BeanPropertyRowMapper<User>(User.class),name);
		return list;
	}
	@Override
	public boolean insertXxdd(Xxdd xd,String time) {//买单后同时添加订单信息
		String sql = "insert xxdd values(?,?,?,?)";
		  int re = jt.update(sql,xd.getX_img(),xd.getX_num(),time,xd.getX_uname());
		return re>0;
	}
	@Override
	public List<Xxdd> selectxd(Xxdd xd) {//订单信息
		String sql = "select * from xxdd where x_uname = ? and x_time = ?";
		List<Xxdd>list = jt.query(sql,new BeanPropertyRowMapper<Xxdd>(Xxdd.class),xd.getX_uname(),xd.getX_time());
		return list;
	}

}
