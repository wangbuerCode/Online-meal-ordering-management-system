package com.rest.dao;

import java.util.List;

import com.rest.model.Mibao;
import com.rest.model.User;

public interface UserPwdDao {
	public List<User> codepwd(String name);//验证原密码
	
	public boolean updatepwd(String name, String pwd);//修改密码
	
	public boolean updatepwd1(int id, String pwd,String code);//设置密保

	public List<User> selectid (String name);//查出id加密保
	
	public List<Mibao> selectmb(int id);
	
}
