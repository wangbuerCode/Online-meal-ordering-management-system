package com.rest.dao;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.rest.model.Mibao;
import com.rest.model.User;
import com.rest.utils.JDBCUtils;

public class UserPwdDaoImpl implements UserPwdDao{
	JdbcTemplate jt = new JdbcTemplate(JDBCUtils.getDataSource());
	@Override
	public List<User> codepwd(String name) {
		String sql ="select u_pwd from user where u_name=?";
		List<User> pwd = jt.query(sql,new BeanPropertyRowMapper<User>(User.class),name);
		return pwd;
	}

	@Override
	public boolean updatepwd(String name,String pwd) {
		String sql = "update user set u_pwd = ? where u_name = ?";
		int bo = jt.update(sql,pwd,name);
		return bo>0;
	}
	@Override
	public boolean updatepwd1(int id, String pwd, String code) {
		String sql = "insert mibao values(?,?,?)";
		 int bo = jt.update(sql,id,pwd,code);
		return bo>0;
	}

	@Override
	public List<User> selectid(String name) {
		String sql = "select u_id from user where u_name = ?";
		List<User> list =jt.query(sql,new BeanPropertyRowMapper<User>(User.class),name);
		return list;
	}

	@Override
	public List<Mibao> selectmb(int id) {
		String sql = "select * from mibao where m_id = ?";
		List<Mibao> list =jt.query(sql,new BeanPropertyRowMapper<Mibao>(Mibao.class),id);
		return list;
	}

	

}
