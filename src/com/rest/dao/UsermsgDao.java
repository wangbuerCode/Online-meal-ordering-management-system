package com.rest.dao;

import java.util.List;

import com.rest.model.User;

public interface UsermsgDao {
	public List<User> selectcode(String name);//查修改的名字是否存在
	public List<User> selectcode2(String telenum);//查修改的号码是否存在
	public List<User> selectcode3(int id);
	public boolean updatemsg(User user);
	public boolean updatedd(String name,String name1);//修改信息要把订单里面的关联数据一起修改
	public boolean updatexd(String name,String name1);//同上
}
