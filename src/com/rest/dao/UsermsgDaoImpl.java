package com.rest.dao;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import com.rest.model.User;
import com.rest.utils.JDBCUtils;

public class UsermsgDaoImpl implements UsermsgDao{
	JdbcTemplate jt = new JdbcTemplate(JDBCUtils.getDataSource());
	
	@Override
	public List<User> selectcode(String name) {
		String sql = "select * from User where u_name = ?";
		List<User>list = jt.query(sql, new BeanPropertyRowMapper<User>(User.class),name);
		return list;
	}
	@Override
	public List<User> selectcode2(String telenum) {
		String sql = "select * from User where u_telenum = ?";
		List<User>list = jt.query(sql, new BeanPropertyRowMapper<User>(User.class),telenum);
		return list;
	}
	
	@Override
	public boolean updatemsg(User user) {
			String sql = "update  User set u_name =? ,u_sex = ?,u_telenum = ?,u_address=? where u_id = ?";
		int re = jt.update(sql,user.getU_name(),user.getU_sex(),user.getU_telenum(),user.getU_address(),user.getU_id());
		return re>0;
	}
	@Override
	public List<User> selectcode3(int id) {
		String sql = "select * from user where u_id=?";
		List<User>list = jt.query(sql, new BeanPropertyRowMapper<User>(User.class),id);
		return list;
	}
	@Override
	public boolean updatedd(String name,String name1) {
		String sql = "update dingdan set d_uname =? where d_uname =?";
		int re = jt.update(sql,name,name1);
		return re>0;
	}
	@Override
	public boolean updatexd(String name, String name1) {
		String sql = "update xxdd set x_uname =? where x_uname =?";
		int re = jt.update(sql,name,name1);
		return re>0;
	}

}
