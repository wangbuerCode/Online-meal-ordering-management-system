package com.rest.model;


public class Dingdan {
	private String d_uname;
	private String d_time;
	private double d_zongjine;
	
	public String getD_uname() {
		return d_uname;
	}
	public void setD_uname(String d_uname) {
		this.d_uname = d_uname;
	}
	public String getD_time() {
		return d_time;
	}
	public void setD_time(String d_time) {
		this.d_time = d_time;
	}
	public double getD_zongjine() {
		return d_zongjine;
	}
	public void setD_zongjine(double d_zongjine) {
		this.d_zongjine = d_zongjine;
	}
	@Override
	public String toString() {
		return "Dingdan [d_uname=" + d_uname + ", d_time=" + d_time + ", d_zongjine=" + d_zongjine + "]";
	}
	
	
	
	
}
