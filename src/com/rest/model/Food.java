package com.rest.model;

public class Food {
	private int f_id;
	private String f_name;
	private String f_img;
	private double f_price;
	private int f_class;
	public int getF_class() {
		return f_class;
	}
	public void setF_class(int f_class) {
		this.f_class = f_class;
	}
	public int getF_id() {
		return f_id;
	}
	public void setF_id(int f_id) {
		this.f_id = f_id;
	}
	public String getF_name() {
		return f_name;
	}
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}
	public String getF_img() {
		return f_img;
	}
	public void setF_img(String f_img) {
		this.f_img = f_img;
	}
	public double getF_price() {
		return f_price;
	}
	public void setF_price(double f_price) {
		this.f_price = f_price;
	}
	@Override
	public String toString() {
		return "Food [f_id=" + f_id + ", f_name=" + f_name + ", f_img=" + f_img + ", f_price=" + f_price + ", f_class="
				+ f_class + "]";
	}
	 
	
	
	
}
