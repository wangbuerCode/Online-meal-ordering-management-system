package com.rest.model;

public class Hfpl {
	private int h_id;
	private String h_name;
	private String h_cment;
	private String h_time;
	private int c_id;
	private String c_name;
	private String c_cment;
	private String c_time;
	public int getH_id() {
		return h_id;
	}
	public int getC_id() {
		return c_id;
	}
	public void setC_id(int c_id) {
		this.c_id = c_id;
	}
	public String getC_name() {
		return c_name;
	}
	public void setC_name(String c_name) {
		this.c_name = c_name;
	}
	public String getC_cment() {
		return c_cment;
	}
	public void setC_cment(String c_cment) {
		this.c_cment = c_cment;
	}
	public String getC_time() {
		return c_time;
	}
	public void setC_time(String c_time) {
		this.c_time = c_time;
	}
	public void setH_id(int h_id) {
		this.h_id = h_id;
	}
	public String getH_name() {
		return h_name;
	}
	public void setH_name(String h_name) {
		this.h_name = h_name;
	}
	public String getH_cment() {
		return h_cment;
	}
	public void setH_cment(String h_cment) {
		this.h_cment = h_cment;
	}
	public String getH_time() {
		return h_time;
	}
	public void setH_time(String h_time) {
		this.h_time = h_time;
	}
	@Override
	public String toString() {
		return "Hfpl [h_id=" + h_id + ", h_name=" + h_name + ", h_cment=" + h_cment + ", h_time=" + h_time + ", c_id="
				+ c_id + ", c_name=" + c_name + ", c_cment=" + c_cment + ", c_time=" + c_time + "]";
	}
	
	
	
	
}
