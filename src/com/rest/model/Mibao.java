package com.rest.model;

public class Mibao {
	private int m_id;
	private String m_wenti;
	private String m_mima;
	public int getM_id() {
		return m_id;
	}
	public void setM_id(int m_id) {
		this.m_id = m_id;
	}
	public String getM_wenti() {
		return m_wenti;
	}
	public void setM_wenti(String m_wenti) {
		this.m_wenti = m_wenti;
	}
	@Override
	public String toString() {
		return "Mibao [m_id=" + m_id + ", m_wenti=" + m_wenti + ", m_mima=" + m_mima + "]";
	}
	public String getM_mima() {
		return m_mima;
	}
	public void setM_mima(String m_mima) {
		this.m_mima = m_mima;
	}
	
	
}
