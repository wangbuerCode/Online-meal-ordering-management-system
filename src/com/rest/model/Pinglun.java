package com.rest.model;

public class Pinglun {
	private int c_id;
	private String c_name;
	private String c_cment;
	private String c_time;
	 
	public int getC_id() {
		return c_id;
	}
	public void setC_id(int c_id) {
		this.c_id = c_id;
	}
	public String getC_name() {
		return c_name;
	}
	public void setC_name(String c_name) {
		this.c_name = c_name;
	}
	public String getC_cment() {
		return c_cment;
	}
	public void setC_cment(String c_cment) {
		this.c_cment = c_cment;
	}
	public String getC_time() {
		return c_time;
	}
	public void setC_time(String c_time) {
		this.c_time = c_time;
	}
	@Override
	public String toString() {
		return "Pinglun [c_id=" + c_id + ", c_name=" + c_name + ", c_cment=" + c_cment + ", c_time=" + c_time + "]";
	}
	
	
	
}
