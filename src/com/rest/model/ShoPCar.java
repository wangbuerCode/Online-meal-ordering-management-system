package com.rest.model;

public class ShoPCar {
	private int s_id;
	private String s_uname;
	private String s_foodimg;
	private double s_foodprice;
	private int s_num;
	private String s_foodname;
	private int s_class;
	public int getS_class() {
		return s_class;
	}
	public void setS_class(int s_class) {
		this.s_class = s_class;
	}
	public String getS_uname() {
		return s_uname;
	}
	public void setS_uname(String s_uname) {
		this.s_uname = s_uname;
	}
	public int getS_id() {
		return s_id;
	}
	public void setS_id(int s_id) {
		this.s_id = s_id;
	}
	public String getS_foodimg() {
		return s_foodimg;
	}
	public void setS_foodimg(String s_foodimg) {
		this.s_foodimg = s_foodimg;
	}
	public double getS_foodprice() {
		return s_foodprice;
	}
	public void setS_foodprice(double s_foodprice) {
		this.s_foodprice = s_foodprice;
	}
	public int getS_num() {
		return s_num;
	}
	public void setS_num(int s_num) {
		this.s_num = s_num;
	}
	public String getS_foodname() {
		return s_foodname;
	}
	public void setS_foodname(String s_foodname) {
		this.s_foodname = s_foodname;
	}
	@Override
	public String toString() {
		return "ShoPCar [s_id=" + s_id + ", s_uname=" + s_uname + ", s_foodimg=" + s_foodimg + ", s_foodprice="
				+ s_foodprice + ", s_num=" + s_num + ", s_foodname=" + s_foodname + ", s_class=" + s_class + "]";
	}
	
	
	
	
	
	
}
