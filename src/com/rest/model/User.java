package com.rest.model;

public class User {
	private int u_id;
	private String u_name;
	private String u_pwd;
	private String u_sex;
	private int u_credits;
	private String u_telenum;
	private String u_address;
	private int u_vip;
	private String v_state;
	private double v_discount;
	
	public String getV_state() {
		return v_state;
	}
	public void setV_state(String v_state) {
		this.v_state = v_state;
	}
	public double getV_discount() {
		return v_discount;
	}
	public void setV_discount(double v_discount) {
		this.v_discount = v_discount;
	}
	public int getU_id() {
		return u_id;
	}
	public void setU_id(int u_id) {
		this.u_id = u_id;
	}
	public String getU_name() {
		return u_name;
	}
	public void setU_name(String u_name) {
		this.u_name = u_name;
	}
	public String getU_pwd() {
		return u_pwd;
	}
	public void setU_pwd(String u_pwd) {
		this.u_pwd = u_pwd;
	}
	public String getU_sex() {
		return u_sex;
	}
	public void setU_sex(String u_sex) {
		this.u_sex = u_sex;
	}
	public int getU_credits() {
		return u_credits;
	}
	public void setU_credits(int u_credits) {
		this.u_credits = u_credits;
	}
	public String getU_telenum() {
		return u_telenum;
	}
	public void setU_telenum(String u_telenum) {
		this.u_telenum = u_telenum;
	}
	public String getU_address() {
		return u_address;
	}
	public void setU_address(String u_address) {
		this.u_address = u_address;
	}
	public int getU_vip() {
		return u_vip;
	}
	public void setU_vip(int u_vip) {
		this.u_vip = u_vip;
	}
	@Override
	public String toString() {
		return "User [u_id=" + u_id + ", u_name=" + u_name + ", u_pwd=" + u_pwd + ", u_sex=" + u_sex + ", u_credits="
				+ u_credits + ", u_telenum=" + u_telenum + ", u_address=" + u_address + ", u_vip=" + u_vip
				+ ", v_state=" + v_state + ", v_discount=" + v_discount + "]";
	}
	
	
	
}
