package com.rest.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rest.dao.DingdanDao;
import com.rest.dao.DingdanDaoImpl;
import com.rest.dao.ShopcDao;
import com.rest.dao.ShopcDaoImpl;
import com.rest.model.Dingdan;
import com.rest.model.ShoPCar;
import com.rest.model.User;
import com.rest.model.Xxdd;

@WebServlet("/DingdanServlet")
public class DingdanServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date now = new Date();
		String time = date.format(now);
		System.out.println(time);
		Dingdan dd = new Dingdan();
		DingdanDao ddd = new DingdanDaoImpl();
		String uname = request.getParameter("name");
		Double zongjine = Double.parseDouble(request.getParameter("zje"));
		String [] imgarr = request.getParameterValues("imgarr");
		String [] caiarr = request.getParameterValues("caiarr");
		//System.out.println(zongjine);
		ShopcDao sd = new ShopcDaoImpl();
		
		List<User>list3 = sd.selectvip(uname);
		List<ShoPCar>list4 = sd.selectall(uname);
		for (int i = 0; i <list4.size(); i++) {
			list4.get(i).getS_foodimg();
			list4.get(i).getS_num();
		}
		dd.setD_uname(uname);
		dd.setD_zongjine(zongjine);
		
		boolean bo = ddd.insertuser(dd,time);//添加订单信息
		if(bo) {
			System.out.println("添加成功");
			for (int i = 0; i < imgarr.length; i++) {
				System.out.println(imgarr[i]);
				System.out.println(caiarr[i]);
				Xxdd xd = new Xxdd();
				xd.setX_img(imgarr[i]);
				xd.setX_num(Integer.parseInt(caiarr[i]));
				xd.setX_uname(uname);
				sd.insertXxdd(xd, time);
			}
			
			ddd.deletemsg(uname);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}