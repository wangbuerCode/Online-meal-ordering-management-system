package com.rest.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rest.dao.DingdanDao;
import com.rest.dao.DingdanDaoImpl;
import com.rest.model.Dingdan;

@WebServlet("/DingdanServlet2")
public class DingdanServlet2 extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		DingdanDao ddd = new DingdanDaoImpl();
		String name = request.getParameter("name");
		System.out.println(name);
		
		List<Dingdan>list = ddd.selectall(name);
		request.setAttribute("ddlist",list);
		request.getRequestDispatcher("dingdan.jsp").forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}