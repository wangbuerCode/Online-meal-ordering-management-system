package com.rest.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rest.dao.FindDao;
import com.rest.dao.FindDaoImpl;
import com.rest.model.Mibao;
import com.rest.model.User;

@WebServlet("/FindServlet")
public class FindServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		User user = new User();
		String name	= request.getParameter("name");
		String tele = request.getParameter("tele");
		//System.out.println(name+":"+tele);
		user.setU_name(name);
		user.setU_telenum(tele);
		FindDao fd = new FindDaoImpl();
		List<User> list = fd.Find(user);
		
	
		
		ObjectMapper om  = new ObjectMapper();
		
		if(list.size()>0) {
			List<Mibao> list1 = fd.Fpwd(list.get(0).getU_id());
			//System.out.println(list1);//
			List<Object> list2=new ArrayList<>();
			list2.add(list);
			list2.add(list1);
			om.writeValue( response.getWriter() , list2);
		}else {
			response.getWriter().write("ʧ��");
		}
		
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
