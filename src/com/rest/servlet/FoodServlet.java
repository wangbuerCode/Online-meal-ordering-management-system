package com.rest.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.rest.dao.FoodDao;
import com.rest.dao.FoodDaoImpl;
import com.rest.dao.LoginDaoImpl;
import com.rest.dao.Logindao;
import com.rest.model.Food;
import com.rest.model.User;

@WebServlet("/FoodServlet")
public class FoodServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		String str = request.getParameter("name");
		System.out.println(str);
		Logindao ld = new LoginDaoImpl();
		if(str.length()==11) {
			List<User>list1 =	ld.ShowLogin1(str);
			request.setAttribute("userlist",list1);
		}else {
			List<User>list1 = ld.ShowLogin(str);
			request.setAttribute("userlist",list1);
		}
		FoodDao fdd = new FoodDaoImpl();
		List<Food> list = fdd.showAll();
		System.out.println(list.toString());
		request.setAttribute("foodlist",list);
		request.getRequestDispatcher("Home.jsp").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");

	}

}