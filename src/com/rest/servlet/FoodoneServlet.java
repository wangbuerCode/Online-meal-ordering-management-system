package com.rest.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rest.dao.FoodDao;
import com.rest.dao.FoodDaoImpl;
import com.rest.model.Food;

@WebServlet("/FoodoneServlet")
public class FoodoneServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println(id);
			FoodDao fd = new FoodDaoImpl();
			List<Food> list = fd.showOne(id);
			System.out.println(list.toString());
		request.setAttribute("foodlist",list);
		ObjectMapper om  = new ObjectMapper();
		om.writeValue( response.getWriter() ,list);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}