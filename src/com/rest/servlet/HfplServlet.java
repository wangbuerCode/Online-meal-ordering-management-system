package com.rest.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rest.dao.HfplDao;
import com.rest.dao.HfplDaoImpl;
import com.rest.model.Hfpl;


@WebServlet("/HfplServlet")
public class HfplServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		

		
	}
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		SimpleDateFormat date = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		Date now = new Date();
		String time = date.format(now);
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String cment = request.getParameter("cment");
		HfplDao hd = new HfplDaoImpl();
		boolean bo =  hd.insertpl(id, name, cment, time);
		if(bo) {
			response.getWriter().write("成功");
		}
	}

}