package com.rest.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rest.dao.LoginDaoImpl;
import com.rest.dao.Logindao;
import com.rest.model.User;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		Logindao ld = new LoginDaoImpl();
		List<User>list = ld.loginname();
//		for (User user : list) {
//			System.out.println(user.getU_name());
//		}
		request.setAttribute("list", list);
		response.sendRedirect("login.jsp");
	}
	List<User> use;
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		User user = new User();
		Logindao ld = new LoginDaoImpl();
		String name = request.getParameter("name");
		String pwd = request.getParameter("pwd");
//		String name = "����";
//		String pwd="285838";
		if(name.length()==11) {
			user.setU_telenum(name);
			user.setU_pwd(pwd);
			use = ld.login1(user);
		}else {
			user.setU_name(name);
			user.setU_pwd(pwd);
			use = ld.login(user);
		}	
		
		if(name=="" || pwd=="") {
			response.getWriter().write("����Ϊ��");
			return;
		}
		
		//System.out.println(use);
		if(use.size()>0) {
			//System.out.println("����"+pwd);
			response.getWriter().write("������ȷ");
			
		}else{
			response.getWriter().write("�������");
		}
		
	}

}