package com.rest.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rest.dao.ShopcDao;
import com.rest.dao.ShopcDaoImpl;
import com.rest.model.ShoPCar;
import com.rest.model.User;

@WebServlet("/MaidanServlet")
public class MaidanServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	String str = "";
	String str1;
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		ShopcDao sd = new ShopcDaoImpl();
		String name = request.getParameter("name");
		System.out.println(name);
		List<ShoPCar>list = sd.Showall(name);
		List<User> list1 = sd.selectvip(name);
		System.out.println(list1.get(0).getV_state());
		System.out.println(list);
		request.setAttribute("showlist",list);
		request.setAttribute("vip",list1);
		//request.setAttribute("name",name);
		request.getRequestDispatcher("ShopCar.jsp").forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		ShopcDao sd = new ShopcDaoImpl();
		ShoPCar sc = new ShoPCar();
		String name = request.getParameter("name");
		String img = request.getParameter("img");
		String uname = request.getParameter("uname");
//		System.out.println(uname);
		Double price = Double.parseDouble(request.getParameter("price"));
//		System.out.println("名字:"+name);
//		System.out.println("图片路径"+img);
//		System.out.println("价格:"+price);
		sc.setS_foodname(name);
		sc.setS_foodimg(img);
		sc.setS_foodprice(price);
		sc.setS_uname(uname);
//		System.out.println("用户名:"+sc.getS_uname());
		List<ShoPCar>list = sd.selectall(uname);
//		System.out.println(list);
//		System.out.println(list.size());
		if(list.size()==0) {
			sd.addCaidan(sc);
		}else if (list!=null&&list.size()>0) {
			for (int i = 0; i < list.size(); i++) {
				if(list.get(i).getS_foodname().equals(name)) {
				str= str+list.get(i).getS_foodname();
				}
			}
			
			if(str.contains(name)) {
				sd.update(name);
			}else {
				sd.addCaidan(sc);
			}
			
		}
//		System.out.println(str);
		
	}

}