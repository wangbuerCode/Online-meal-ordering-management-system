package com.rest.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rest.dao.PinglunDao;
import com.rest.dao.PinglunDaoImpl;
@WebServlet("/MyhfServlet1")
public class MyhfServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		String name= request.getParameter("name");
		String time = request.getParameter("time");
		System.out.println(name+time);
		PinglunDao pld = new PinglunDaoImpl();
		boolean bo = pld.delecthf(name, time);
		if(bo) {
			response.getWriter().write("�ɹ�");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}