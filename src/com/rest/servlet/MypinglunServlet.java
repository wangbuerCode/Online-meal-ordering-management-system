package com.rest.servlet;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rest.dao.HfplDao;
import com.rest.dao.HfplDaoImpl;
import com.rest.dao.LoginDaoImpl;
import com.rest.dao.Logindao;
import com.rest.dao.PinglunDao;
import com.rest.dao.PinglunDaoImpl;
import com.rest.model.Hfpl;
import com.rest.model.Pinglun;
import com.rest.model.User;
@WebServlet("/MypinglunServlet")
public class MypinglunServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		String name = request.getParameter("name");
		System.out.println(name);
		Logindao ld = new LoginDaoImpl();
	    List<User>list = ld.ShowLogin(name);
		System.out.println(list);
		request.setAttribute("msglist",list);
		
		PinglunDao pld = new PinglunDaoImpl();
		List<Pinglun>list1 = pld.showmy(name);
		
		
		HfplDao hd = new HfplDaoImpl();
	    List<Hfpl>list2 = hd.showhf();
	    
	    List<Hfpl>list3 = pld.showhf(name);
	    request.setAttribute("myhf", list3);
	    
	    request.setAttribute("hflist",list2);
		
		request.setAttribute("mypllist",list1);
		
		request.getRequestDispatcher("mypinglun.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		int id = Integer.parseInt(request.getParameter("id"));
		PinglunDao pld = new PinglunDaoImpl();
		boolean bo = pld.delectmypl(id);
		if(bo) {
			response.getWriter().write("�ɹ�");
		}
	}

}