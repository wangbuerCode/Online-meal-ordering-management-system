package com.rest.servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rest.dao.HfplDao;
import com.rest.dao.HfplDaoImpl;
import com.rest.dao.LoginDaoImpl;
import com.rest.dao.Logindao;
import com.rest.dao.PinglunDao;
import com.rest.dao.PinglunDaoImpl;
import com.rest.model.Hfpl;
import com.rest.model.Pinglun;
import com.rest.model.User;

@WebServlet("/PinglunServlet")
public class PinglunServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		PinglunDao pld = new PinglunDaoImpl();
		List<Pinglun>list = pld.showpl();
//		for (Pinglun pinglun : list) {
//			System.out.println(pinglun.toString());
//		}
		String name = request.getParameter("name");
		Logindao ld = new LoginDaoImpl();
	    List<User>list1 = ld.ShowLogin(name);
//		System.out.println(list1);
	    HfplDao hd = new HfplDaoImpl();
	    List<Hfpl>list2 = hd.showhf();
	    request.setAttribute("hflist",list2);
		request.setAttribute("msglist",list1);
		request.setAttribute("pllist",list);
		
		request.getRequestDispatcher("pinglun.jsp").forward(request, response);
		}
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		SimpleDateFormat date = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
		Date now = new Date();
		String time = date.format(now);
		System.out.println(time);
		String name = request.getParameter("name");
		String cment = request.getParameter("cment");
		System.out.println(name);
		System.out.println(cment);
		PinglunDao pld = new PinglunDaoImpl();
		if(cment!=""&&cment!=null) {
		boolean bo = pld.insertpl(name, cment, time);
		if(bo) {
			response.getWriter().write("成功");
		}
		}
		
	}

}