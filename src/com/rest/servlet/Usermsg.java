package com.rest.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rest.dao.LoginDaoImpl;
import com.rest.dao.Logindao;
import com.rest.dao.UsermsgDao;
import com.rest.dao.UsermsgDaoImpl;
import com.rest.model.User;

@WebServlet("/Usermsg")
public class Usermsg extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		String name = request.getParameter("name");
		System.out.println(name);
		Logindao ld = new LoginDaoImpl();
	    List<User>list = ld.ShowLogin(name);
		System.out.println(list);
		request.setAttribute("msglist",list);
		request.getRequestDispatcher("usermsg.jsp").forward(request, response);
		
		}
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		UsermsgDao umd = new UsermsgDaoImpl();
		User user = new User();
		int id = Integer.parseInt(request.getParameter("id"));
		String name = request.getParameter("name");
		String sex = request.getParameter("sex");
		String telenum = request.getParameter("telenum");
		String address = request.getParameter("address");
		user.setU_id(id);
		user.setU_name(name);
		user.setU_sex(sex);
		user.setU_telenum(telenum);
		user.setU_address(address);
		List<User>list = umd.selectcode3(id);
		boolean bo = umd.updatemsg(user);
		if(bo) {
			umd.updatedd(name,list.get(0).getU_name());
			umd.updatexd(name,list.get(0).getU_name());
			System.out.println("�޸ĳɹ�");
		}
	
		
		
	
	}

}