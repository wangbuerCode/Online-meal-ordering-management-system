package com.rest.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rest.dao.ShopcDao;
import com.rest.dao.ShopcDaoImpl;
import com.rest.model.Xxdd;

@WebServlet("/XxddServlet")
public class XxddServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		ShopcDao sd = new ShopcDaoImpl();
		String time = request.getParameter("time");
		String name = request.getParameter("name");
//		System.out.println(time);
//		System.out.println(name);
		Xxdd xd = new Xxdd();
		xd.setX_uname(name);
		xd.setX_time(time);
		List<Xxdd>list = sd.selectxd(xd);
		for (Xxdd xxdd : list) {
			System.out.println(xxdd.toString());
		}
		request.setAttribute("xdlist",list);
		request.getRequestDispatcher("xxdingdan.jsp").forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}