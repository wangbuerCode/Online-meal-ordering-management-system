package com.rest.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.rest.dao.LoginDaoImpl;
import com.rest.dao.Logindao;
import com.rest.dao.ZhuceDao;
import com.rest.dao.ZhuceDaoImpl;
import com.rest.model.User;



@WebServlet("/ZhuceServlet")
public class ZhuceServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		
		HttpSession session = request.getSession();
		String code=(String)session.getAttribute("checkcode");
		String name = request.getParameter("name");//用户名
		String pwd = request.getParameter("pwd");//密码
		String pwd1 = request.getParameter("pwd1");//确认密码
		String tele = request.getParameter("tele");//手机号
		String code1 = request.getParameter("code");//验证码
		//System.out.println("1"+code);
		//System.out.println("2"+code1);
			
		if(code1!=""&&!code.equalsIgnoreCase(code1)) {//忽略大小写
			response.getWriter().write("验证码不正确");
		}
		
		User user = new User();
		user.setU_name(name);
		if(pwd.equals(pwd1)) {
			user.setU_pwd(pwd);
		}else {
			response.getWriter().write("两次密码不同");
			return;
		}
		user.setU_telenum(tele);
		ZhuceDao zd = new ZhuceDaoImpl();
		if(name!=null||pwd!=null||pwd1!=null||tele!=null||code1!=null) {
			boolean bo = zd.insertuser(user);
			 if(bo) {
				 response.getWriter().write("注册成功");
				
			 }
		}
		 
		
		
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		
		Logindao li = new LoginDaoImpl();
		List<User> list = li.loginname();
		
		
		if(request.getParameter("name")!=null||request.getParameter("name")!="") {
			String name = request.getParameter("name");
			for (User user : list) {
				if (name!=""&&name!= null&&name.equals(user.getU_name())) {
					response.getWriter().write("改用户名已存在");
					return;
				}
			}
		}
		
		
		
		if(request.getParameter("tele")!=""||request.getParameter("tele")!=null) {
			String tele = request.getParameter("tele");
			//System.out.println(tele.getBytes().length);
			//System.out.println(tele.length());
			for (User user : list) {
				if(tele!= null&&tele!=""&&tele.equals(user.getU_telenum())) {
					response.getWriter().write("已经注册");
					return;
				}else if(tele!= null&&tele!=""&&tele.getBytes().length!=11) {
					//System.out.println("非法");
					response.getWriter().write("非法格式");
					return;
				}
			}
			}else {
				System.out.println(11);
			}	
		
			
		
	
		
	}

}