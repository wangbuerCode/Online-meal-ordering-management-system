package com.rest.utils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.sql.DataSource;

import com.alibaba.druid.pool.DruidDataSourceFactory;

public class JDBCUtils {
	private static DataSource ds;
	
	static {
		try {
			Properties pro=new Properties();
			//加载数据库配置文件
			InputStream is= JDBCUtils.class.getClassLoader().getResourceAsStream("druid.properties");
			pro.load(is);
			
			//获取数据库连接池对象DS
			ds=DruidDataSourceFactory.createDataSource(pro);
			
		} catch (IOException e) {
			
			e.printStackTrace();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public static DataSource getDataSource() {
		return ds;
	}
	
	public static Connection getConnection() throws SQLException {
		return ds.getConnection();
	}
	
	public static void close(Statement smt,Connection con) {
		close(null,smt,con);
	}
	public static void close(ResultSet rst, Statement smt,Connection con) {
		if(rst!=null) {
			try {
				rst.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}
		if(smt!=null) {
			try {
				smt.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}
		if(con!=null) {
			try {
				con.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}
	}

}
